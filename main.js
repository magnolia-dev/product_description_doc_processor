// Importing dependencies
const jsdom = require("jsdom");
const csv = require('csv-parser');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const fs = require('fs');

// Loading the Google Doc into a DOM processor
let html = fs.readFileSync('doc.html', 'utf8');
const dom = new jsdom.JSDOM(html);

// A container for descriptions found in the Google Doc
let descriptions = {};

// A regex matching product titles
let productTitlePattern = /^\d+\. (.*)/;

// Processing every DOM element in the Google Doc that
// could be a product title, and finding their matching
// product descriptions.
dom.window.document.querySelectorAll('span.c5').forEach(e => {

  // Skipping elements that don't look like product titles
  if (!productTitlePattern.test(e.textContent)) {
    return;
  }

  // Removing the leading product number, and trimming extra white space
  let cleanTitle = e.textContent.replace(productTitlePattern, '$1').trim();

  let productTitleContainer = e.parentElement;
  let vendorContainer = productTitleContainer.nextElementSibling;
  let descriptionContainer = vendorContainer.nextElementSibling;
  let description = descriptionContainer.firstElementChild.textContent;

  descriptions[cleanTitle] = description;
});

// A container for the rows of the new CSV
let newRows = [];

// Reading the existing CSV to find the rows that each product description
// belongs to.
let csvReader = fs.createReadStream('imports.csv').pipe(csv());

csvReader.on('data', (row) => {
  let title = row['Title'];
  if (descriptions[title]) {
    let newRow = { ...row };
    newRow['Body (HTML)'] = descriptions[title];
    newRows.push(newRow);
  }
  else {
    // Maybe push old row...
  }
})

csvReader.on('end', () => {
  console.log({ newRows });
  console.log(newRows.length);
  writeNewCsv(newRows);
});


function writeNewCsv(rows) {
  const [first] = rows;

  const header = Object.keys(first).map(key => ({
    id: key,
    title: key,
  }));

  const writer = createCsvWriter({
      path: 'processed_imports.csv',
      header,
  });
  
  writer.writeRecords(rows)
    .then(() => {
        console.log('...Done');
    });
}