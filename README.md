
## About

A small NodeJS project that collects product titles and descriptions from a Google Doc, and uses the data to create a CSV for bulk import into Shopify.

## Structure

* **doc.html** - A product description Google Doc that has been downloaded via `File -> Download -> Web Page` from the Google Docs UI.

* **main.js** - The script that triggers the automation.

## Instructions

* `npm install` - Installs NodeJS dependencies

* `node main` - Begins processing
